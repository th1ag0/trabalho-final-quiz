package com.example.quizapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.quizapp.db.dao.PlayerDao
import com.example.quizapp.entities.Player

@Database(version = 1, entities = arrayOf(Player::class),exportSchema = true)
abstract class AppDatabase: RoomDatabase() {
    abstract fun playerDao():PlayerDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        private val lock = Any()

        fun getInstance(context: Context): AppDatabase {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "Quiz.db")
                        .build()
                }
                return INSTANCE!!
            }
        }
    }
}