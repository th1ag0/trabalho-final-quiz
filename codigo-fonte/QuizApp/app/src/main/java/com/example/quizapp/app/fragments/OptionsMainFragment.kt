package com.example.quizapp.app.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.Navigation.findNavController
import com.example.quizapp.R
import kotlinx.android.synthetic.main.fragment_options_main.*


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class OptionsMainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_options_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = findNavController(view)

        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            navController.navigate(R.id.mainFragment)
        }

        btPlay.setOnClickListener {
            navController.navigate(R.id.gameContentFragment)
        }
        btRanking.setOnClickListener {
            navController.navigate(R.id.rankingContentFragment)
        }
    }
}
