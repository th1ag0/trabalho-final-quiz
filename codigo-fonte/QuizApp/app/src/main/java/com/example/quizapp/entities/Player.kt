package com.example.quizapp.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "players")
data class Player constructor(

//    var name:String,
    var login:String,
    var password:String
) {
    @PrimaryKey(autoGenerate = false)
    var id: Long = 0
    var name:String = ""

    constructor(name:String, login:String, password:String): this(login, password){
        this.name = name
    }

    constructor(name:String, login:String, password:String,id:Long): this(name, login, password){
        this.id = id
    }


    override fun toString(): String {
        return "id: ${id}, login: ${login}, senha: ${password}"
    }
}