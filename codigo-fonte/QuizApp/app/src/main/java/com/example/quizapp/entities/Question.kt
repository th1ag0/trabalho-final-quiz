package com.example.quizapp.entities

data class Question(
    var id: Long,
    var statement:String,
    var alternative1: String,
    var alternative2: String,
    var alternative3: String,
    var alternative4: String,
    var answer: String
) {
    var flag = false

    override fun equals(other: Any?) =
        other != null && (this === other || (this.id != 0L && this.id == (other as Question).id))
}