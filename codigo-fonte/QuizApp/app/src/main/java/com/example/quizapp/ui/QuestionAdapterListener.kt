package com.example.quizapp.ui

import android.view.View

interface QuestionAdapterListener {

    fun toOptionsMain(view: View)
    fun setRanking(punctuation:Int, view: View)
}