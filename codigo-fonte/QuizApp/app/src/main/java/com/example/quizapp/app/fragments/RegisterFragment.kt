package com.example.quizapp.app.fragments


import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.quizapp.PlayerViewModel

import com.example.quizapp.R
import com.example.quizapp.entities.Player
import com.example.quizapp.network.PlayerService
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class RegisterFragment : Fragment() {

    lateinit var service: PlayerService
    private val viewModel: PlayerViewModel by activityViewModels()

    var contextActivity: Context? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureRetrofit()

        btRegister.setOnClickListener {
            val player = Player(edNameRegister.text.toString(),edLoginRegister.text.toString(),edPasswordRegister.text.toString())

            service.validatePlayer(player).enqueue(object : Callback<Boolean>{
                override fun onFailure(call: Call<Boolean>, t: Throwable) {
                    Log.e("MSG", "${t.message}", t)
                }

                override fun onResponse(call: Call<Boolean>, response: Response<Boolean>) {
                    val playerResult = response.body()
                    if (playerResult != null){
                        if (playerResult){
                            Log.d("MSG", "valido")
                            insertPlayer(player)
                        }else{
                            val msg = view.context.resources.getString(R.string.title_invalid_login)
                            tvMsgregister.setText("${msg}!")
                        }
                    }
                }
            })
        }
    }

    private fun insertPlayer(player: Player){
            service.insert(player).enqueue(object : Callback<Player> {
                override fun onFailure(call: Call<Player>, t: Throwable) {
                    Log.e("MSG", "${t.message}", t)
                }

                override fun onResponse(call: Call<Player>, response: Response<Player>) {
                    val playerResult = response.body()
                    if (playerResult != null){
                        Log.d("MSG", playerResult.toString())
                        viewModel.insert(playerResult)
                        findNavController().navigate(R.id.optionsMainFragment)
                    }
                }
            })
    }

    private fun configureRetrofit() {
        val gson = GsonBuilder().create()

        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.0.2.2:8085/quiz-api/public/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        service = retrofit.create<PlayerService>(PlayerService::class.java)
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)

        this.contextActivity = context
    }
}
