package com.example.quizapp.network

import com.example.quizapp.entities.Player
import com.example.quizapp.entities.Question
import com.example.quizapp.entities.Ranking
import retrofit2.Call
import retrofit2.http.*

interface RankingService {

    @GET("ranking")
    fun getRanking(): Call<List<Ranking>>

    @GET("ranking/find/{id}")
    fun findById(@Path("id")playerId: Long): Call<Ranking>

    @POST("ranking")
    fun insert(@Body ranking: Ranking): Call<Ranking>

    @PUT("ranking/{id}")
    fun update(@Path("id")playerId: Long, @Body ranking: Ranking): Call<Ranking>
}