package com.example.quizapp

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.quizapp.db.source.QuizRepository
import com.example.quizapp.entities.Player

class PlayerViewModel(application: Application): AndroidViewModel(application) {

    private val mRepository: QuizRepository
    internal val playerLog: LiveData<List<Player>>

    init {
        mRepository = QuizRepository(application)
        playerLog = mRepository.allPlayers
    }

    fun getPlayer(): LiveData<List<Player>> {
        return playerLog
    }
    fun insert(player: Player) {
        mRepository.insert(player)
    }

    fun delete(player: Player) {
        mRepository.delete(player)
    }
}