package com.example.quizapp.entities

data class Ranking constructor(
    var playerId:Player,  // alterado (era int)
    var punctuation:Int
) {

    var idRanking:Long = 0

    constructor(idRanking:Long, punctuation: Int, playerId: Player): this (playerId, punctuation){
        this.idRanking = idRanking
    }
}