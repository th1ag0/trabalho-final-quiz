package com.example.quizapp.app.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.quizapp.PlayerViewModel

import com.example.quizapp.R
import com.example.quizapp.entities.Player
import kotlinx.android.synthetic.main.fragment_main.*
import kotlin.system.exitProcess

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MainFragment : Fragment() {
    private val viewModel: PlayerViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.delete(Player("", "", "", 136))

        btStart.setOnClickListener {
            val navController = Navigation.findNavController(view)
            viewModel.getPlayer().observe(this,
                Observer<List<Any>> { players ->
                    if (players.size == 0){
                        navController.navigate(R.id.loginFragment)

                    }else{
                        Log.d("MSG", players.get(0).toString())
                        navController.navigate(R.id.optionsMainFragment
                        )
                    }
                })
        }
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            exitProcess(0)
        }
    }


}
