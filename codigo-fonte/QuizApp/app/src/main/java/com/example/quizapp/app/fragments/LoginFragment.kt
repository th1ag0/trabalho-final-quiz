package com.example.quizapp.app.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.quizapp.PlayerViewModel
import com.example.quizapp.R
import com.example.quizapp.entities.Player
import com.example.quizapp.network.PlayerService
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {

    lateinit var service:PlayerService
    private val viewModel: PlayerViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    private fun authenticate(player:Player) {

        service.authenticatePlayer(player).enqueue(object : Callback<Player>{
            override fun onFailure(call: Call<Player>, t: Throwable) {
                Log.e("MSG", "${t.message}", t)
            }

            override fun onResponse(call: Call<Player>, response: Response<Player>) {
                val player = response.body()
                if (player!=null){
                    Log.d("MSG", "true${player.toString()}")
                    viewModel.insert(player)
                    findNavController().navigate(R.id.optionsMainFragment)
                }else{
                    tvMsg.setError("")
                    tvMsg.setText(context!!.resources.getString(R.string.msg_user_invalid))
                }
            }
        })
    }

     override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configRetrofit()

        btLogin.setOnClickListener {
            authenticate(Player(edLoginAuth.text.toString(), edPasswordAuth.text.toString()))
        }
        linkRegister.setOnClickListener {
            findNavController().navigate(R.id.registerFragment)
        }
    }

    private fun configRetrofit() {
        val gson = GsonBuilder().create()

        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.0.2.2:8085/quiz-api/public/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        service = retrofit.create<PlayerService>(PlayerService::class.java)
    }
}
