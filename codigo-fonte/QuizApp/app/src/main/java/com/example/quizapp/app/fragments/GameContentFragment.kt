package com.example.quizapp.app.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.quizapp.R
import com.example.quizapp.entities.Question
import com.example.quizapp.network.QuestionService
import com.example.quizapp.ui.QuestionAdapter
import com.example.quizapp.ui.QuestionAdapterListener
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_game_content.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class GameContentFragment : Fragment(), QuestionAdapterListener {


    lateinit var service: QuestionService
    lateinit var adapter: QuestionAdapter

    override fun setRanking(punctuation: Int, view: View) {

        val navController = Navigation.findNavController(view)
        var bundle = bundleOf("punctuation" to punctuation)
        navController.navigate(R.id.rankingContentFragment,bundle)
    }

    override fun toOptionsMain(view: View) {
        val navController = Navigation.findNavController(view)
        navController.navigate(R.id.optionsMainFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureRetrofit()
        loadQuestions()

    }

    private fun configureRetrofit() {
        val gson = GsonBuilder().create()

        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.0.2.2:8085/quiz-api/public/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        service = retrofit.create<QuestionService>(QuestionService::class.java)
    }

    private fun loadQuestions(){
        service.getQuestions().enqueue(object : Callback<List<Question>> {
            override fun onResponse(call: Call<List<Question>>, response: Response<List<Question>>) {
                val questions = response.body()
                if (questions != null)
                loadRecyclerView(questions)
            }

            override fun onFailure(call: Call<List<Question>>, t: Throwable) {
                Log.e("ERRO", t.message, t)
            }
        })
    }

    private fun loadRecyclerView(questions: List<Question>) {
//      if (questions != null) {
            adapter = QuestionAdapter(questions.toMutableList(),this)
            listQuestion.adapter = adapter
            listQuestion.layoutManager = LinearLayoutManager(
                this.context,
                RecyclerView.VERTICAL, false
        )
//      }
    }
}
