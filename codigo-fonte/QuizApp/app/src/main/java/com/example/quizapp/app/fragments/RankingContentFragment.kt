package com.example.quizapp.app.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.quizapp.PlayerViewModel

import com.example.quizapp.R
import com.example.quizapp.entities.Player

import com.example.quizapp.entities.Ranking
import com.example.quizapp.network.PlayerService

import com.example.quizapp.network.RankingService
import com.example.quizapp.ui.RankingAdapter
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_ranking_content.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class RankingContentFragment : Fragment() {

    lateinit var service: RankingService
    lateinit var adapter: RankingAdapter
    private val viewModel: PlayerViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ranking_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureRetrofit()
        loadRanking()
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            Navigation.findNavController(view).navigate(R.id.optionsMainFragment)
        }
        val punctuation = arguments!!.getInt("punctuation")
        if (punctuation != 0) {
            saveRanking(punctuation)
        }

    }

    private fun setRanking(id:Long, player: Player, punctuation: Int){
        service.update(id,Ranking(player, punctuation)).enqueue(  // alterado (era id = int)
            object : Callback<Ranking> {
                override fun onResponse(call: Call<Ranking>, response: Response<Ranking>) {
                    adapter.setRanking(response.body()!!)
                }

                override fun onFailure(call: Call<Ranking>, t: Throwable) {
                    Log.e("MSG", t.message, t)
                }
            })
    }
    private fun insertRanking(player: Player, punctuation: Int){
        val ranking = Ranking(player,punctuation)   // alterado
        service.insert(ranking).enqueue(
            object : Callback<Ranking> {
                override fun onResponse(call: Call<Ranking>, response: Response<Ranking>) {
                    adapter.insertRanking(response.body()!!)
                    Log.d("MSG", response!!.body().toString())
                }

                override fun onFailure(call: Call<Ranking>, t: Throwable) {
                    Log.e("MSG", t.message, t)
                }
            })
    }
    private fun getRankingByPlayer(player:Player, punctuation: Int) {
        service.findById(player.id).enqueue(
            object : Callback<Ranking> {

                override fun onResponse(call: Call<Ranking>, response: Response<Ranking>) {
                    val rankingResponse = response.body()
                    if (rankingResponse != null) {
                        if(rankingResponse.idRanking.toInt() == 0) {
                            Log.d("MSG", "insert${player.id}")
                            insertRanking(player, punctuation)

                        }else{
                            Log.d("MSG", "set${rankingResponse.idRanking}")
                            val punctuationPrevious = rankingResponse.punctuation
                            if (punctuation > 0){
                                setRanking(rankingResponse.idRanking, player,punctuation+punctuationPrevious)
                            }
                        }
                    }
                }
                override fun onFailure(call: Call<Ranking>, t: Throwable) {
                    Log.e("MSG", t.message, t)
                }
            })
    }

    private fun saveRanking(punctuation: Int){

        //pega as informaçoes do jogador
        viewModel.getPlayer().observe(this,
            Observer<List<Player>> { players ->
                if (players.size != 0){
                    getRankingByPlayer(players[0], punctuation)
                }
            })

    }

    private fun configureRetrofit() {
        val gson = GsonBuilder().create()

        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.0.2.2:8085/quiz-api/public/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        service = retrofit.create<RankingService>(RankingService::class.java)

    }

    private fun loadRanking(){
        service.getRanking().enqueue(object : Callback<List<Ranking>> {
            override fun onResponse(call: Call<List<Ranking>>, response: Response<List<Ranking>>) {
                val listRanking = response.body()
                if (listRanking != null)
                    loadRecyclerView(listRanking.toMutableList())
            }

            override fun onFailure(call: Call<List<Ranking>>, t: Throwable) {
                Log.e("MSG", t.message, t)
            }
        })
    }

    private fun loadRecyclerView(list: MutableList<Ranking>) {
        list.sortBy { it.punctuation }
        list.reverse()
        adapter = RankingAdapter(list)
        listRanking.adapter = adapter
        listRanking.layoutManager = LinearLayoutManager(
            this.context,
            RecyclerView.VERTICAL, false
        )
    }
}
