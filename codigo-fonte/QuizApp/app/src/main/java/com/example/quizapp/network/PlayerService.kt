package com.example.quizapp.network

import com.example.quizapp.entities.Player
import com.example.quizapp.entities.Question
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.*

interface PlayerService {

    @POST("player")
    fun insert(@Body player:Player): Call<Player>

    @GET("players")
    fun getPlayers():Call<List<Player>>

    @POST("player/auth")
    fun authenticatePlayer(@Body player:Player): Call<Player>

    @POST("player/validate")
    fun validatePlayer(@Body player:Player): Call<Boolean>

    @GET("player/find/{id}")
    fun getPlayerById(@Path("id") id:Int):Call<Player>
}