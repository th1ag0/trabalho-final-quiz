package com.example.quizapp.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.quizapp.entities.Player


@Dao
interface PlayerDao {
    @Query("SELECT * FROM players ORDER BY id DESC")
    fun getAll(): LiveData<List<Player>>

    @Insert
    fun insert(player: Player): Long

    @Update
    fun update(player: Player)

    @Delete
    fun delete(player: Player)

//    @Query("SELECT * FROM tarefas WHERE id = :id LIMIT 1")
//    fun findById(id:Int): Player?
}