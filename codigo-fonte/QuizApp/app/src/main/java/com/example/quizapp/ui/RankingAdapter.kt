package com.example.quizapp.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.quizapp.R
import com.example.quizapp.entities.Ranking
import kotlinx.android.synthetic.main.card_score_user.view.*

class RankingAdapter(var lisRanking: MutableList<Ranking>):
    RecyclerView.Adapter<RankingAdapter.RankingViewHolder>(){

    fun setRanking(ranking: Ranking){
        var position = 0
        for (i in lisRanking.indices){
            if(lisRanking[i].idRanking == ranking.idRanking){
                position = i
            }
        }
        lisRanking.set(position,ranking)
        lisRanking.sortBy { it.punctuation }
        lisRanking.reverse()
        notifyDataSetChanged()
    }

    fun insertRanking(ranking: Ranking){
        lisRanking.add(ranking)
        notifyItemInserted(lisRanking.size-1)
        lisRanking.sortBy { it.punctuation }
        lisRanking.reverse()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        RankingViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.card_score_user, parent, false)
        )

    override fun getItemCount()= lisRanking.size

    override fun onBindViewHolder(holder: RankingViewHolder, position: Int) {
        var ranking = lisRanking[position]
        holder.fillUI(ranking)
    }

    inner class RankingViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun fillUI(ranking: Ranking){
            itemView.tvNameRanking.setText("${ranking.playerId.name}")
            itemView.tvScoreRanking.setText(ranking.punctuation.toString())
        }
    }
}