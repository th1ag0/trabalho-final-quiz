package com.example.quizapp.db.source

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.quizapp.db.AppDatabase
import com.example.quizapp.db.dao.PlayerDao
import com.example.quizapp.entities.Player


class QuizRepository internal constructor(application: Application){

    private val playerDao: PlayerDao
    internal val allPlayers: LiveData<List<Player>>

    init {
        val db = AppDatabase.getInstance(application)
        playerDao = db.playerDao()
        allPlayers = playerDao.getAll()
    }

    fun insert(player: Player) {
        AsyncPlayerAdd(playerDao).execute(player)
    }
//    fun update(player: Player) {
//        AsyncPlayer(playerDao).execute(player)
//    }

    fun delete(player: Player){
        AsyncPlayerRemove(playerDao).execute(player)
    }


    private class AsyncPlayerAdd internal constructor(private val mAsyncPlayerDao: PlayerDao) : AsyncTask<Player, Void, Void>() {

        override fun doInBackground(vararg params: Player): Void? {
            mAsyncPlayerDao.insert(params[0])
            return null
        }
    }
    private class AsyncPlayerRemove internal constructor(private val mAsyncPlayerDao: PlayerDao) : AsyncTask<Player, Void, Void>() {

        override fun doInBackground(vararg params: Player): Void? {
            mAsyncPlayerDao.delete(params[0])
            return null
        }
    }
}