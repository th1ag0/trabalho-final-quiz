package com.example.quizapp.ui

import android.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import com.example.quizapp.R
import com.example.quizapp.entities.Question
import kotlinx.android.synthetic.main.card_question.view.*




class QuestionAdapter (var questions: MutableList<Question>, var listener: QuestionAdapterListener):
    RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder>(){

    var qtde = 0
    var acertos = 0
    var erros = 0
    private var questionAnswered:Question? = null

    fun remove(id:Int){
        questions.removeAt(id)
        notifyItemRemoved(id)
    }

    private fun setQuestionAnswered(question: Question){
        questionAnswered = question
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        QuestionViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.card_question, parent, false)
        )

    override fun getItemCount()= questions.size

    override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {

        holder.itemView.tvQuestion.setText(questions[position].statement)
        holder.itemView.rbAlternative1.setText(questions[position].alternative1)
        holder.itemView.rbAlternative2.setText(questions[position].alternative2)
        holder.itemView.rbAlternative3.setText(questions[position].alternative3)
        holder.itemView.rbAlternative4.setText(questions[position].alternative4)
        if (!questions[position].flag){
            holder.itemView.radioGroup.clearCheck()
        }
        holder.fillUI(questions[position])
    }

    inner class QuestionViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun fillUI(question: Question){

//            itemView.radioGroup.setOnCheckedChangeListener { group, checkedId ->
//                val p = questions.indexOf(question)
//
//            }

            itemView.btAnswer.setOnClickListener {

                setQuestionAnswered(question)
                val selectId = itemView.radioGroup.checkedRadioButtonId
                val radioButton = itemView.findViewById<RadioButton>(selectId)

                if(radioButton != null){
                    val position = questions.indexOf(question)
                    if(radioButton.text.equals(question.answer)){
                        Log.d("MSG", "ACERTOU")
                        acertos++
                    }else{
                        Log.d("MSG", "ERROU${radioButton.text}/${question.answer}")
                        erros++
                    }
                    qtde++
                    if(qtde == 10){
                        val builder = AlertDialog.Builder(itemView.context)
                        val msg = itemView.context.resources.getString(R.string.title_hits)
                        builder
                            .setTitle("${msg}")
                            .setMessage("${acertos}").create().show()
                        listener.setRanking(acertos, itemView)
                    }
                    questions[position].flag = true
                    remove(position)
                }
            }
        }
    }
}