package com.example.quizapp.network

import com.example.quizapp.entities.Question
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface QuestionService {

    @GET("questions")
    fun getQuestions(): Call<List<Question>>

}