# Quiz 

![Quiz](img/question.png)

# O Jogo

O Quiz consiste em um jogo de perguntas e respostas onde o usuário se cadastra no app e pode iniciar uma partida. Cada partida iniciada contém um total de 10 questões sobre diversos assuntos. Ao final, o jogador pode visualizar sua pontuação. Durante o acesso ao jogo, o usuário também pode ver as melhores pontuações do jogo.

# Dados utilizados 

* Os dados dos usuários e das questões serão armazenados remotamente em um banco de dados. 
* Esses dados serão consumidos pelo app por meio de um web service.
* Os dados de um usuário cadastrado no app também fica salvo localmente para o acesso ao app.

# Modelagem dos dados

![Diagrama](img/diagrama.png)


